# Awesome Tox

A list of Tox projects & resources. Inspired by other awesome lists.

Feel free to contribute :smile:

## Bots

* [zetok/Lee](https://github.com/zetok/Lee) – inhumanly intelligent markov bot written in Rust; supports groupchats and 1v1 conversations. [<img src="https://travis-ci.org/zetok/Lee.svg?branch=master">](https://travis-ci.org/zetok/Lee)
* [zetok/tox-neurological-warfare-bot](https://github.com/zetok/tox-neurological-warfare-bot) – spambot that delivers all the spam you desire upon request. [<img src="https://travis-ci.org/zetok/tox-neurological-warfare-bot.svg?branch=master">](https://travis-ci.org/zetok/tox-neurological-warfare-bot)

## Clients

* [Antidote-for-Tox/Antidote](https://github.com/Antidote-for-Tox/Antidote) – iOS Tox client. [<img src="https://travis-ci.org/Antidote-for-Tox/Antidote.svg?branch=master">](https://travis-ci.org/Antidote-for-Tox/Antidote)
* [Antox/Antox](https://github.com/Antox/Antox) – Android Tox client. [<img src="https://travis-ci.org/Antox/Antox.svg?branch=master">](https://travis-ci.org/Antox/Antox)
* [qTox/qTox](https://github.com/qTox/qTox) – most feature-rich Tox client, written in C++/Qt. [<img src="https://travis-ci.org/qTox/qTox.svg?branch=master">](https://travis-ci.org/qTox/qTox)
* [JFreegman/toxic](https://github.com/JFreegman/toxic) – Ncurses-based CLI client. [<img src="https://travis-ci.org/JFreegman/toxic.svg?branch=master">](https://travis-ci.org/JFreegman/toxic)
* [uTox/uTox](https://github.com/uTox/uTox) – "lightweight" Tox client written in "STR8C". [<img src="https://travis-ci.org/uTox/uTox.svg?branch=master">](https://travis-ci.org/uTox/uTox)
* [ratox](http://git.z3bra.org/ratox/log.html) – file-system based client, originally started by the 2f30 project.

## Documentation

* [zetok/tox-spec](https://github.com/zetok/tox-spec) – refined Tox protocol specification. [<img src="https://travis-ci.org/zetok/tox-spec.svg?branch=master">](https://travis-ci.org/zetok/tox-spec)

## toxcore implementations

* [TokTok/hstox](https://github.com/TokTok/hstox) – Haskell implementation. [<img src="https://travis-ci.org/TokTok/hstox.svg?branch=master">](https://travis-ci.org/TokTok/hstox)
* [TokTok/c-toxcore](https://github.com/TokTok/c-toxcore) – implementation of Tox protocol written in C. [<img src="https://travis-ci.org/TokTok/c-toxcore.svg?branch=master">](https://travis-ci.org/TokTok/c-toxcore)
* [zetok/tox](https://github.com/zetok/tox) – zetox toxcore implementation written in Rust. [<img src="https://travis-ci.org/zetok/tox.svg?branch=master">](https://travis-ci.org/zetok/tox)

## toxcore wrappers

* [saneki/node-toxcore](https://github.com/saneki/node-toxcore) – Node.js wrapper for toxcore. [<img src="https://travis-ci.org/saneki/node-toxcore.svg?branch=master">](https://travis-ci.org/saneki/node-toxcore)
* [codedust/go-tox](https://github.com/codedust/go-tox) – Go wrapper for toxcore. [<img src="https://travis-ci.org/codedust/go-tox.svg?branch=master">](https://travis-ci.org/codedust/go-tox)
* [abbat/pytoxcore](https://github.com/abbat/pytoxcore) – Python wrapper for toxcore. [<img src="https://travis-ci.org/abbat/pytoxcore.svg?branch=master">](https://travis-ci.org/abbat/pytoxcore)

## Utilities

* [zetok/tox-add-pubkey](https://github.com/zetok/tox-add-pubkey) – a command line utility to add public key to the `.tox` profile. [<img src="https://travis-ci.org/zetok/tox-add-pubkey.svg?branch=master">](https://travis-ci.org/zetok/tox-add-pubkey)
* [gjedeer/tuntox](https://github.com/gjedeer/tuntox) – forwarding TCP connections over Tox. [<img src="https://travis-ci.org/gjedeer/tuntox.svg?branch=master">](https://travis-ci.org/gjedeer/tuntox)
* [cleverca22/toxvpn](https://github.com/cleverca22/toxvpn) – point to point tunneling (VPN) over Tox. [<img src="https://travis-ci.org/cleverca22/toxvpn.svg?branch=master">](https://travis-ci.org/cleverca22/toxvpn)
